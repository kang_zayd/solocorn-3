import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:solocorn3/bloc/data_diri_bloc.dart';
import 'package:solocorn3/injector.dart';
import 'package:solocorn3/model/RequestDataDiri.dart';
import 'package:solocorn3/model/http_response.dart';
import 'package:solocorn3/page/list_page.dart';

class InputPage extends StatefulWidget {
  const InputPage({Key? key}) : super(key: key);

  @override
  State<InputPage> createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  final DataDiriBloc _dataDiriBloc = inject<DataDiriBloc>();
  final TextEditingController _nikController = TextEditingController();
  final TextEditingController _namaController = TextEditingController();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _alamatController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(centerTitle: true, title: const Text("Input Page")),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            customForm("NIK", Icons.credit_card,
                controller: _nikController, helper: "Pastikan NIK harus angka", textInputType: TextInputType.number),
            customForm("Nama", Icons.person, helper: "Pastikan nama sesuai KTP", controller: _namaController),
            customForm("Username", Icons.mail, controller: _usernameController),
            customForm("Alamat", Icons.add_business, controller: _alamatController),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
                onPressed: () async {
                  List<RequestDataDiri> param = [
                    RequestDataDiri(
                        nik: _nikController.text.toString(),
                        nama: _namaController.text.toString(),
                        alamat: _alamatController.text.toString(),
                        username: _usernameController.text.toString())
                  ];
                  ResponseState post =
                      await _dataDiriBloc.postDataDiri(jsonEncode(param.map((e) => e.toJson()).toList()));
                  if (post.status == Status.SUCCESS) {
                    Navigator.push(context, MaterialPageRoute(builder: (ctx) => const ListPage()));
                  } else {
                    var snack = SnackBar(content: Text("Gagal ${post.message}"));
                    ScaffoldMessenger.of(context).showSnackBar(snack);
                  }
                },
                child: const Text("Simpan"))
          ],
        ),
      ),
    );
  }

  /*var snack = const SnackBar(content: Text("Berhasil"));
                  ScaffoldMessenger.of(context).showSnackBar(snack);*/
  customForm(String title, IconData iconData,
      {String? helper, TextInputType? textInputType, TextEditingController? controller}) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: TextFormField(
        keyboardType: textInputType,
        controller: controller,
        decoration: InputDecoration(
            prefixIcon: Icon(iconData),
            hintText: title,
            helperText: helper,
            contentPadding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 10.0),
            border: const OutlineInputBorder(borderSide: BorderSide(width: 1.0, color: Colors.grey))),
      ),
    );
  }
}
