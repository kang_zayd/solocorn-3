import 'package:flutter/material.dart';
import 'package:solocorn3/bloc/data_diri_bloc.dart';
import 'package:solocorn3/injector.dart';
import 'package:solocorn3/model/DataDiriModel.dart';
import 'package:solocorn3/model/http_response.dart';

class ListPage extends StatefulWidget {
  const ListPage({Key? key}) : super(key: key);

  @override
  State<ListPage> createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  List bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni"];
  final DataDiriBloc _dataDiriBloc = inject<DataDiriBloc>();

  @override
  void initState() {
    super.initState();
    _dataDiriBloc.getDataDiri();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("List Page")),
      body: StreamBuilder<ResponseState<List<ItemsDataDiri>?>>(
          stream: _dataDiriBloc.listDataDiri,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              var resp = snapshot.data!;
              switch (resp.status) {
                case Status.LOADING:
                  return const Center(child: CircularProgressIndicator());
                case Status.ERROR:
                  return Text(resp.message);
                case Status.SUCCESS:
                  var data = resp.data!;
                  if (data.isNotEmpty) {
                    return ListView.builder(
                      itemBuilder: (context, index) {
                        return ListTile(
                            title: Text(data[index].nama??"-"),
                            subtitle: Text(data[index].nik??"-"),
                            leading: Icon(Icons.person),
                            trailing: Icon(Icons.keyboard_arrow_right));
                      },
                      itemCount: data.length,
                    );
                  }
                  return const Text("Data Belum Tersedia");
              }
            } else {
              return const SizedBox();
            }
          }),
    );
  }
}
