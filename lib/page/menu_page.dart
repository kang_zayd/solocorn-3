import 'package:flutter/material.dart';
import 'package:solocorn3/page/home_page.dart';
import 'package:solocorn3/page/list_page.dart';

class MenuPage extends StatefulWidget {
  const MenuPage({Key? key}) : super(key: key);

  @override
  State<MenuPage> createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (ctx) => const MyHomePage(title: "Solocorn")));
                },
                child: const Text("Home Page")),
            const SizedBox(height: 16.0),
            ElevatedButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (ctx) => const ListPage()));
                },
                child: const Text("List Page"))
          ],
        ),
      ),
    );
  }
}
