import 'package:flutter/material.dart';

class ListStaticPage extends StatefulWidget {
  const ListStaticPage({Key? key}) : super(key: key);

  @override
  State<ListStaticPage> createState() => _ListStaticPageState();
}

class _ListStaticPageState extends State<ListStaticPage> {
  List bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemBuilder: (context, index) {
          return ListTile(title: Text(bulan[index]),
          leading: Icon(Icons.person),
              trailing: Icon(Icons.keyboard_arrow_right));
        },
        itemCount: bulan.length,
      ),
    );
  }
}
