import 'package:flutter/material.dart';
import 'package:solocorn3/page/input_page.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Solocorn"),
        ),
        body: Padding(
            padding: const EdgeInsets.all(16),
            child: Stack(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [const FlutterLogo(), const Text("Flutter")],
                      ),
                    ),
                    TextFormField(),
                    const SizedBox(height: 20),
                    TextFormField(
                      decoration: InputDecoration(
                          prefixIcon: const Icon(Icons.mail),
                          suffixIcon: const Icon(Icons.arrow_forward),
                          hintText: "Masukkan Email",
                          helperText: "Email tidak boleh kosong",
                          labelText: "Email",
                          contentPadding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 10.0),
                          border: OutlineInputBorder(
                              borderSide: const BorderSide(width: 1.0, color: Colors.grey),
                              borderRadius: BorderRadius.circular(100))),
                    ),
                    const SizedBox(height: 20),
                    customForm("Email"),
                    customForm("Nama"),
                    customForm("Alamat", helperText: "Pastikan sesuai dengan KTP anda"),
                    customForm("Nomor telepon"),
                    const SizedBox(height: 20),
                    TextFormField(
                      obscureText: true,
                      decoration: const InputDecoration(
                          hintText: "Password",
                          contentPadding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 10.0),
                          border: OutlineInputBorder(borderSide: BorderSide(width: 1.0, color: Colors.grey))),
                    ),
                    const SizedBox(height: 20),
                    const Text("Forgot Password"),
                    ElevatedButton(
                      onPressed: () {
                        Navigator.push(context, MaterialPageRoute(builder: (ctx) => const InputPage()));
                      },
                      child: const Text("Login"),
                    )
                  ],
                ),
                const Align(alignment: Alignment.bottomCenter, child: Text("New User?Create Account")),
              ],
            )) // This trailing comma makes auto-formatting nicer for build methods.
        );
  }

  customForm(String title, {String? helperText}) {
    return TextFormField(
      decoration: InputDecoration(
          suffixIcon: const Icon(Icons.mail),
          hintText: title,
          helperText: helperText,
          contentPadding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 10.0),
          border: const OutlineInputBorder(borderSide: BorderSide(width: 1.0, color: Colors.grey))),
    );
  }
}
