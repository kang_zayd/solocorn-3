import 'package:rxdart/rxdart.dart';
import 'package:solocorn3/dio_client.dart';
import 'package:solocorn3/model/DataDiriModel.dart';
import 'package:solocorn3/model/http_response.dart';

class DataDiriBloc {
  final DioClient _client;

  DataDiriBloc(this._client);

  final BehaviorSubject<ResponseState<List<ItemsDataDiri>?>> _listDataDiri = BehaviorSubject();

  Stream<ResponseState<List<ItemsDataDiri>?>> get listDataDiri => _listDataDiri.asBroadcastStream();

  final BehaviorSubject<ResponseState> _post = BehaviorSubject();

  Stream<ResponseState> get post => _post.asBroadcastStream();

  Future getDataDiri() async {
    _listDataDiri.add(ResponseState(status: Status.LOADING, message: 'Mengambil Data'));
    final response = await _client.create(url: "https://crudapi.co.uk/api/v1/tes", method: Method.GET);
    ResponseState<List<ItemsDataDiri>?> state;
    if (response.status) {
      state = ResponseState(
          status: Status.SUCCESS, message: response.message, data: DataDiriModel.fromJson(response.data).items);
    } else {
      state = ResponseState(status: Status.ERROR, message: response.message, code: response.code);
    }
    _listDataDiri.add(state);
  }

  Future postDataDiri(param) async {
    _post.add(ResponseState(status: Status.LOADING, message: 'Mengirim Data'));
    ResponseState state;
    try {
      final response =
          await _client.create(url: "https://crudapi.co.uk/api/v1/tes", method: Method.POSTJSON, json: param);
      if (response.status) {
        state = ResponseState(status: Status.SUCCESS, message: response.message);
      } else {
        state = ResponseState(status: Status.ERROR, message: response.message, code: response.code);
      }
    } catch (e) {
      state = ResponseState(status: Status.ERROR, message: e.toString());
    }
    _post.add(state);
    return state;
  }
}
