enum Status { LOADING, SUCCESS, ERROR }

class HttpResponse {
  int? code;
  bool status;
  String message;
  dynamic data;

  HttpResponse({required this.status, required this.message, this.data, this.code});
}

class ResponseState<T> {
  int? code;
  Status status;
  String message = "";
  T? data;

  ResponseState({required this.status, required this.message, this.data, this.code});
}

