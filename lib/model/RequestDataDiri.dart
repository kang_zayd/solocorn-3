/// nama : "Said"
/// alamat : "Mayang"
/// nik : "875029875298375"
/// username : "said"

class RequestDataDiri {
  RequestDataDiri({
      this.nama, 
      this.alamat, 
      this.nik, 
      this.username,});

  RequestDataDiri.fromJson(dynamic json) {
    nama = json['nama'];
    alamat = json['alamat'];
    nik = json['nik'];
    username = json['username'];
  }
  String? nama;
  String? alamat;
  String? nik;
  String? username;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['nama'] = nama;
    map['alamat'] = alamat;
    map['nik'] = nik;
    map['username'] = username;
    return map;
  }

}