/// cursor : null
/// items : [{"_created":1686461481.886595,"_data_type":"tes","_is_deleted":false,"_modified":1686461481.886608,"_self_link":"https://crudapi.co.uk/api/v1/tes/8fcf5ebe-af43-41f2-ba56-d23fc39ad752","_user":"b1de509f-b5cd-4eca-9627-ffb0a7ff1976","_uuid":"8fcf5ebe-af43-41f2-ba56-d23fc39ad752","alamat":"Mayang","nama":"Said","nik":"875029875298375","username":"said"}]
/// next_page : null

class DataDiriModel {
  DataDiriModel({
      this.cursor, 
      this.items, 
      this.nextPage,});

  DataDiriModel.fromJson(dynamic json) {
    cursor = json['cursor'];
    if (json['items'] != null) {
      items = [];
      json['items'].forEach((v) {
        items?.add(ItemsDataDiri.fromJson(v));
      });
    }
    nextPage = json['next_page'];
  }
  dynamic cursor;
  List<ItemsDataDiri>? items;
  dynamic nextPage;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['cursor'] = cursor;
    if (items != null) {
      map['items'] = items?.map((v) => v.toJson()).toList();
    }
    map['next_page'] = nextPage;
    return map;
  }

}

/// _created : 1686461481.886595
/// _data_type : "tes"
/// _is_deleted : false
/// _modified : 1686461481.886608
/// _self_link : "https://crudapi.co.uk/api/v1/tes/8fcf5ebe-af43-41f2-ba56-d23fc39ad752"
/// _user : "b1de509f-b5cd-4eca-9627-ffb0a7ff1976"
/// _uuid : "8fcf5ebe-af43-41f2-ba56-d23fc39ad752"
/// alamat : "Mayang"
/// nama : "Said"
/// nik : "875029875298375"
/// username : "said"

class ItemsDataDiri {
  ItemsDataDiri({
      this.created, 
      this.dataType, 
      this.isDeleted, 
      this.modified, 
      this.selfLink, 
      this.user, 
      this.uuid, 
      this.alamat, 
      this.nama, 
      this.nik, 
      this.username,});

  ItemsDataDiri.fromJson(dynamic json) {
    created = json['_created'];
    dataType = json['_data_type'];
    isDeleted = json['_is_deleted'];
    modified = json['_modified'];
    selfLink = json['_self_link'];
    user = json['_user'];
    uuid = json['_uuid'];
    alamat = json['alamat'];
    nama = json['nama'];
    nik = json['nik'];
    username = json['username'];
  }
  double? created;
  String? dataType;
  bool? isDeleted;
  double? modified;
  String? selfLink;
  String? user;
  String? uuid;
  String? alamat;
  String? nama;
  String? nik;
  String? username;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_created'] = created;
    map['_data_type'] = dataType;
    map['_is_deleted'] = isDeleted;
    map['_modified'] = modified;
    map['_self_link'] = selfLink;
    map['_user'] = user;
    map['_uuid'] = uuid;
    map['alamat'] = alamat;
    map['nama'] = nama;
    map['nik'] = nik;
    map['username'] = username;
    return map;
  }

}