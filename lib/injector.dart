import 'package:get_it/get_it.dart';
import 'package:solocorn3/bloc/data_diri_bloc.dart';
import 'package:solocorn3/dio_client.dart';

final GetIt inject = GetIt.I;

Future<void> setupInjection() async {
  //Components
  inject.registerSingleton(DioClient());

  //ViewModels
  inject.registerLazySingleton(() => DataDiriBloc(inject()));
}
